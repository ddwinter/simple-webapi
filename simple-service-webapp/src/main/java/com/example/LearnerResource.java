package com.example;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.example.dao.LearnersDao;
import com.example.objects.Learner;
import com.example.objects.Learners;

@Path("learners")
public class LearnerResource {
	
	LearnersDao data = LearnersDao.getInstance();
	private static final Logger LOGGER = Logger.getLogger(LearnerResource.class);

	public LearnerResource()
	{

	
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//Get all learners
	public Learners getLearners() {
		Learners learners = data.getAllLearners();
		LOGGER.debug(learners);
		return learners;
	}
	
	@Path ("{learnerId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Learner getLearner(@PathParam("learnerId") final String learnerId) {
		final Learner learner = data.getLearner(learnerId);
		
		return learner;
		
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Learner createLearner(final Learner learner) {
		
		return data.addLearner(learner);
		
	}

	@Path ("{learnerId}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	//Update a learner, success:0, fault:-1
	public int updateLearner(@PathParam("learnerId") final String learnerId, final Learner learner) {
		if (learner == null) {
			return -1;
		}
		
		int result = data.updateLearner(learnerId, learner);
		return result;
		
	}
	
	@Path ("{learnerId}")
	@DELETE
	//remove a learner
	public int deleteLearner(@PathParam("learnerId") final String learnerId) {
	
		return data.deleteLearner(learnerId);
	}
	
	@Path ("groupid:{groupId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	//search learns by groupId
	public Learners getLearnersByGroupId(@PathParam("groupId") final int groupId) {
		Learners learners = data.getAllLearners();
		
		return learners.getLearnersByGroupId(groupId);

	}
	
}
