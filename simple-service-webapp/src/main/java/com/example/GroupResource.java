package com.example;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.example.dao.LearnersDao;
import com.example.objects.*;

@Path("groups")
public class GroupResource {
	
	LearnersDao data = LearnersDao.getInstance();
	
	public GroupResource() {
		
	}
	
    @GET  
    @Produces(MediaType.APPLICATION_JSON )  
    public Groups getGroups() {  
        Groups list = data.getGroups();
        
        return list;  
    }  


}
