package com.example.objects;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;  
@XmlRootElement  
public class Learner implements Serializable{  
      
   // private static final long serialVersionUID = 1L;  
  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName = "";  
      
    private int groupId = 0;  
    
    private String userId = "";
    
    @XmlAttribute(name = "LearnerId")
    public String getUserId() {  
        return userId;  
    }
    
    public void setUserId(String id) {  
        userId = id;  
    } 
    
    @XmlAttribute(name = "LearnerName")
    public String getUserName() {  
        return userName;  
    }  
  
    public void setUserName(String userName) {  
        this.userName = userName;  
    }  
  
    @XmlAttribute(name = "GroupId")
    public int getGroupId() {  
        return groupId;  
    }  
  
    public void setGroupId(int id) {  
        this.groupId = id;  
    }  
      
}  