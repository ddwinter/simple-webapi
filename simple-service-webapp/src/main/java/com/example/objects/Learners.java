package com.example.objects;

import java.util.UUID;    
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement (name = "learners") 
public class Learners implements Serializable{  
      

	private static final long serialVersionUID = 4951580623547930311L;
	private List<Learner>  learnerList = new ArrayList<Learner>();
 
	
    @XmlElement(name = "learner")
    @XmlElementWrapper
  
    public List<Learner> getLearnerList() {  
        return learnerList;
    }  
    
    //add a learner
    public Learner addLearner(Learner learner){
    	String id = UUID.randomUUID().toString();
    	learner.setUserId(id);
    	learnerList.add(learner);
    	return learner;
    }
    
    //find position of a learner by id
    private int getIndex(String learnerId){
    	int index= -1;
    	for(int i=0; i< learnerList.size(); i++) {
    		String id = learnerList.get(i).getUserId();
    		if (id.equals(learnerId)) {
    			index = i;
    		}
    	}
    	
    	return index;
    }
    
    //Search a learner
    public Learner getLearner(String learnerId)
    {
    	int index = getIndex(learnerId);
    	if (index >= 0)
    		return learnerList.get(index);
    	else
    		return null;
    	
    }
    
    //update learner
    public int updateLearner(String id, Learner learner){
    	Learner currentLearner = this.getLearner(id);
    	if (currentLearner ==null) return -1;
    	else{
    		currentLearner.setGroupId(learner.getGroupId());
    		currentLearner.setUserName(learner.getUserName());
    		return 0;
    	}
    }
    
    //Remove a learner
    public int deleteLearner(String learnerId){
    	int index = getIndex(learnerId);
    	if (index >=0) {
    		learnerList.remove(index);
    		return 0;
    	}
    	else return -1;
    }
    
  public Learners getLearnersByGroupId(int groupId){
	  
		Learners rows = new Learners();
		for(int i=0; i<learnerList.size(); i++){ 
			Learner row = learnerList.get(i);
			if( row.getGroupId() == groupId){
				rows.addLearner(row);
			}
		}

		return rows;
  }

}  