package com.example.objects;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement  
public class Group implements Serializable{
    
	private static final long serialVersionUID = 1L;

	private String groupName = "";  
    
	private int groupId = 0;  

  
  @XmlAttribute(name = "GroupId")
  public int getId() {  
      return groupId;  
  }
  
  public void setId(int id) {  
      groupId = id;  
  } 
  
  @XmlAttribute(name = "groupName")
  public String getName() {  
      return groupName;  
  }  

  public void setName(String userName) {  
      this.groupName = userName;  
  }  
    
}  