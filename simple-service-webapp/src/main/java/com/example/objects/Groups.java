package com.example.objects;
    
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;  

@XmlRootElement (name = "groups") 
public class Groups implements Serializable{  
      
	private static final long serialVersionUID = 3590260315882497974L;
	private List<Group>  groupList = new ArrayList<Group>();
 
	
    @XmlElement(name = "group")
    @XmlElementWrapper
  
    public List<Group> getGroupList() {  
        return groupList;
    }  
    
    //add a learner
    public void add(Group group){
    	groupList.add(group);
    	
    }
      
}  