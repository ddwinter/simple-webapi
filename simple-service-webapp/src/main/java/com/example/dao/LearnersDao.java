package com.example.dao;

import com.example.objects.Group;
import com.example.objects.Groups;
import com.example.objects.Learner;
import com.example.objects.Learners;
import org.springframework.stereotype.Repository;

@Repository
public class LearnersDao {
	private static LearnersDao single = new LearnersDao();
	private static Learners learners;
	private static Groups groups;
	private LearnersDao()
	{
		learners = new Learners();
		Learner learner = new Learner();
		learner.setUserName("Tony");
		learner.setGroupId(1);
		learners.addLearner(learner);
		
		
		learner = new Learner();
		learner.setUserName("Hogan");
		learner.setGroupId(1);
		learners.addLearner(learner);
		
		learner = new Learner();
		learner.setUserName("Chaoyang");
		learner.setGroupId(1);
		learners.addLearner(learner);
		
		learner = new Learner();
		learner.setUserName("Duke");
		learner.setGroupId(2);
		learners.addLearner(learner);
		
		learner = new Learner();
		learner.setUserName("Allen");
		learner.setGroupId(2);
		learners.addLearner(learner);
		
		learner = new Learner();
		learner.setUserName("Grace");
		learner.setGroupId(3);
		learners.addLearner(learner);
		
    	groups = new Groups();
    	Group group = new Group();
    	group.setId(1);
    	group.setName("RS");
    	groups.add(group);
    	
    	group = new Group();
    	group.setId(2);
    	group.setName("Accessment");
    	groups.add(group);
    	
    	group = new Group();
    	group.setId(3);
    	group.setName("QA");
    	groups.add(group);
		
	}
	
	public static LearnersDao getInstance(){
		return single;
	}
	
	//Get all learners
	public Learners getAllLearners()
	{
		return learners;
	}
	
	//Add a learner
	public Learner addLearner(Learner learner){
		return learners.addLearner(learner);
	}
	
	//Search a learner
	public Learner getLearner(final String learnerId) {
		final Learner learner = learners.getLearner(learnerId);
		return learner;
		
	}
	
	//update learner
    public int updateLearner(String id, Learner learner){
    	int result = learners.updateLearner(id, learner);
    		return result;
    	
    }
    
    //Remove a learner
    public int deleteLearner(String learnerId){
    		return learners.deleteLearner(learnerId);
    }
    
    public Groups getGroups(){

    	
    	return groups;
    }
    
    //Search learners by groupId
  	public Learners getLearnersByGroupId(final int groupId) {
  		Learners rows = new Learners();
  		for(int i=0; i<learners.getLearnerList().size(); i++){
  			Learner row = learners.getLearnerList().get(i);
  			if( row.getGroupId() == groupId){
  				rows.addLearner(row);
  			}
  		}

  		return rows;
  		
  	}
  
}
