### What is this repository for? ###

* This app provide web api to below user story:

	https://jira.trstone.com/browse/RSA-1374

* It's accessed by the Front end app project:

   	https://ddwinter@bitbucket.org/chinacompass/learnangular.git

### Main technologies ###

*   It's main technology is Jersey

### How do I get set up? ###

If you are using Spring Tools Suite

1. Remove or change file name for RS's settings.xml in .m2 root folder

2. in sts, Maven install

3. Configure tomcat as your local server

4. Run as -- run on server, input "compile" in goal field. Or add below node under <build> parent node in your pom.xml:
	<defaultGoal>compile</defaultGoal> 
      
You should see the home page